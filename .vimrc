" Windows-specific
""""""""""""""""""
set belloff=all
set backspace=2
set clipboard=unnamed

" GUI
"""""
set guifont=Liberation\ Mono:h14
"set guioptions -=m
"set guioptions +=T
set winaltkeys=no
colorscheme desert

" Plain text
""""""""""""
set colorcolumn=61
set cursorline
set encoding=utf-8
set hlsearch
set incsearch
set laststatus=2
set linebreak
set noexpandtab         " Tabs are tabs
set number
set ruler
set spell
set tabstop=4           " Numof visual spaces per <TAB>
set tw=60
set wildmenu
set wrap

filetype on
syntax enable
syntax on

" Python
""""""""
autocmd FileType python call SetPythonOptions()

function SetPythonOptions()
    setlocal colorcolumn=81
    setlocal expandtab
    setlocal nospell
    setlocal nowrap
    setlocal shiftwidth=4
    setlocal smarttab
    setlocal smartindent
    setlocal textwidth=0
    filetype indent on
endfunction

" Keyboard
""""""""""
inoremap <A-h> <C-o>h
inoremap <A-j> <C-o>j
inoremap <A-k> <C-o>k
inoremap <A-l> <C-o>l

inoremap <A-f> <C-o><C-f>
inoremap <A-b> <C-o><C-b>

inoremap <A-4> <C-o>$
inoremap <A-0> <C-o>0

inoremap <A-g> <Del>
inoremap <A-t> <Backspace>
inoremap <A-n> <CR>

nnoremap <A-h> h
nnoremap <A-j> j
nnoremap <A-k> k
nnoremap <A-l> l

nnoremap <A-f> <C-f>
nnoremap <A-b> <C-b>

nnoremap <A-4> $
nnoremap <A-0> 0

nnoremap <F12> iLog <C-R>=strftime("%Y-%m-%d-T-%I:%M")<CR> 
